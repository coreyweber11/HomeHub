import { DomSanitizer } from '@angular/platform-browser';

export class JsonFeed{
    title: string
    author: string
    date: string
    thumbnail: string
    url: string
    media: any
    html: any
    gif: any

   constructor(feed: any){
        this.title = feed.title;
        this.author = feed.author; 
        this.date = feed.created
        this.thumbnail = feed.thumbnail;
        this.url = feed.url;
        if(feed.media){
            this.media = feed.media.oembed;
            this.html = feed.media.oembed.html;
            this.thumbnail = null;
        }/*else if(feed.preview){
            if(feed.preview.images[0].resolutions != null){
                console.log("using resolutions");
                this.gif = feed.preview.images[0].resolutions[1].url;
            }else{
                this.gif = feed.preview.images[0].source; 
            }
            this.thumbnail = null;
        }*/
        if(feed.thumbnail == "default"){
            this.thumbnail = "../../assets/images/Reddit_Logo.png"
        }
    } 

    
 }
