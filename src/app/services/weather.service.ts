import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Weather, DailyWeather } from '../model/weather';
import 'rxjs/Rx';

const APPID = 'd4a4fcb3ee493e0372d28467bc89a366';

@Injectable()
export class WeatherService {

  private baseUrl='http://api.openweathermap.org/data/2.5/';
  private cityName: string="Charlottetown";

  constructor(private http: Http) { }

  getWeatheritemsbyCity(): any{
    return new Promise(resolve => {
    	 return this.http.get(this.baseUrl +'weather?q='+ this.cityName +'&appid='+ APPID +'&units=metric')
    	 .map(res => res.json())
       .subscribe(data => {
          resolve(this.extractData(data));
        });
    });
  }

  getWeatherForecast(): any{
    return new Promise(resolve => {
      this.http.get(this.baseUrl+'forecast/daily?q='+ this.cityName +'&appid='+ APPID +'&units=metric')
      .map(response => response.json())
      .subscribe(data => {
          resolve(this.extractListData(data));
      });
    });
  }

  private extractData(data: any) {
    console.log(data);
    return  new Weather(data.name, data.weather[0].description ,data.main.temp, data.main.temp_max, data.main.temp_min, data.dt);
  }

   private extractDailyData(data: any) {
    console.log(data);
    return  new DailyWeather(this.cityName, data.weather[0].description, data.temp.day, data.temp.max, data.temp.min, data.dt);
  }

  private extractListData(data: any){
    console.log(data);
    var forecast:any = [];

    for (var i=0; i < 7; i++)
    {  
      forecast.push(this.extractDailyData(data.list[i]));
    }  
    return forecast || {};
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    // if (error instanceof Response) {
    //   const body = error.json() || '';
    //   const err = body.error || JSON.stringify(body);
    //   errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    // } else {
      errMsg = error.message ? error.message : error.toString();
    //}
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
