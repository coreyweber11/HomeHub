import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'weather-forecast',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.css']
})
export class WeatherForecastComponent implements OnInit {

  weatherForecast:any;
  errorMessage: any;

  constructor(public weatherService :WeatherService) { }

  ngOnInit() {
    this.getForecast();
  }

   getForecast(){
     this.weatherService.getWeatherForecast().then(data => {
        this.weatherForecast = data; 
        console.log(data);
      },error =>  this.errorMessage = <any>error,            
     );
  }
}
