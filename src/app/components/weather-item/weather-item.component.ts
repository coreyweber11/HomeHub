import { Component, OnInit, Input } from '@angular/core';
// round.pipe.ts
import {Pipe, PipeTransform} from "@angular/core";
 
/**
 *
 */
@Pipe({name: 'round'})
export class RoundPipe implements PipeTransform {
    /**
     *
     * @param value
     * @returns {number}
     */
    transform(value: number): number {
        return Math.round(value);
    }
}

@Component({
  selector: 'weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.scss']
})
export class WeatherItemComponent implements OnInit {
   @Input('WeatherItem') weather: any;

  constructor() { 

  }

  ngOnInit() {
    
  }

}
