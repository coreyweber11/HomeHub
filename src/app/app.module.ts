import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule }    from '@angular/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { RoundPipe } from './components/weather-item/weather-item.component';
import { FeedCardComponent } from './components/feed-card/feed-card.component';
import { FeedService } from './services/feed.service';
import { WeatherService } from './services/weather.service';
import { ClockService } from './services/clock.service';
import { WeatherItemComponent } from './components/weather-item/weather-item.component';
import { Clock } from './components/clock/clock.component';
import { WeatherForecastComponent } from './components/weather-forecast/weather-forecast.component';
import { FeedComponent } from './components/feed/feed.component';
import { SafePipe } from './pipes/sanitize.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FeedCardComponent,
    WeatherItemComponent,
    Clock,
    WeatherForecastComponent,
    FeedComponent,
    RoundPipe,
    SafePipe
  ],
  imports: [
    BrowserModule, HttpModule, FormsModule
  ],
  providers: [FeedService, WeatherService, ClockService],
  bootstrap: [AppComponent]
})
export class AppModule { }
